export interface LeaveType {
  id:   string;
  name: string;
  type: boolean;
}
