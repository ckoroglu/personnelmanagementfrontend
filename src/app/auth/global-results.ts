import {Injectable} from '@angular/core';
import {Employee} from '../models/resultmodels/employee';

@Injectable()
export class GlobalResults {

  private _account: Employee;

  constructor() {
  }

  get account(): Employee {
    return this._account;
  }

  set account(account: Employee) {
    this._account = account;
  }

}
