import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {Dashboard} from '../../models/resultmodels/dashboard';

declare var AdminLTE: any;

@Component({
  selector: 'app-admin-dashboard1',
  templateUrl: './admin-dashboard1.component.html',
  styleUrls: ['./admin-dashboard1.component.css']
})
export class AdminDashboard1Component implements OnInit {

  dashboardInfo: Dashboard = null;
  userRole = 'user';

  constructor(private apiService: ApiService) {
    this.userRole = localStorage.getItem('role');
  }

  ngOnInit() {
    // Update the AdminLTE layouts
    AdminLTE.init();
    this.apiService.getDashboardInfo().subscribe(res => {
      this.dashboardInfo = res.data;
    });
  }

}
