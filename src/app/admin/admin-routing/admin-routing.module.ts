import { AdminDashboard1Component } from './../admin-dashboard1/admin-dashboard1.component';
import { AdminComponent } from './../admin.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {AuthGuard} from '../../auth/auth.guard';
import {TaskComponent} from '../task/task.component';
import {DepartmentComponent} from '../department/department.component';
import {LeavetypeComponent} from '../leavetype/leavetype.component';
import {UserLeaveComponent} from '../user-leave/user-leave.component';
import {ProjectsComponent} from '../projects/projects.component';
import {EmployeeComponent} from '../employee/employee.component';
import {LeaveRequestComponent} from '../leave-request/leave-request.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        canActivate: [AuthGuard],
        path: 'app',
        component: AdminComponent,
        children: [
          {
            path: '',
            redirectTo: 'home',
            pathMatch: 'full'
          },
          {
            path: 'home',
            component: AdminDashboard1Component
          },
          {
            path: 'tasks',
            component: TaskComponent
          },
          {
            path: 'departments',
            component: DepartmentComponent
          },
          {
            path: 'userleaves',
            component: UserLeaveComponent
          },
          {
            path: 'project',
            component: ProjectsComponent
          },
          {
            path: 'employee',
            component: EmployeeComponent
          },
          {
            path: 'leavetype',
            component: LeavetypeComponent
          },
          {
            path: 'leaverequest',
            component: LeaveRequestComponent
          },
        ]
      }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule { }
