export interface Project {
  id:          string;
  name:        string;
  description: string;
  status:      number;
  startDate:   string;
  endDate:     string;
  cratedBy:    string;
}
