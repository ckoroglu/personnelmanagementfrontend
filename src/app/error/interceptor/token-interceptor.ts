import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse, HttpHeaders
} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import 'rxjs/add/observable/empty';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private routerService: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let headers: HttpHeaders = new HttpHeaders();

    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Access-Control-Allow-Origin', '*');
    headers = headers.append('Access-Control-Allow-Headers', 'Origin');
    headers = headers.append('Access-Control-Allow-Credentials', 'true');
    headers = headers.append('Access-Control-Allow-Methods', 'DELETE, HEAD, GET, OPTIONS, POST, PUT');
    headers = headers.append('Access-Control-Allow-Headers', 'Content-Type, Content-Range, Content-Disposition, Content-Description');
    headers = headers.append('Access-Control-Max-Age', '1728000');

    if (localStorage.getItem('token')) {
      headers = headers.append('Authorization', 'Basic ' + localStorage.getItem('token'));
    }

    const clonedRequest = request.clone({headers: headers});
    return next.handle(clonedRequest).pipe(catchError((error, caught) => {
      this.handleError(error);
      return of(error);
    }) as any);

  }

  private handleError(err: HttpErrorResponse): Observable<any> {
    if (err.status === 401) {
      localStorage.removeItem('token');
      localStorage.removeItem('role');
      this.routerService.navigate(['/login']);
      return of(err.message);
    }
    return Observable.empty();
  }

}

