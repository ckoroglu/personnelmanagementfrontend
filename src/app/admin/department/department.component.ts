import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {Department} from '../../models/resultmodels/department';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit {

  departments: Department[] = [];

  editActive: string = null;
  editForm: FormGroup;

  createActive = false;
  createForm: FormGroup;

  constructor(private apiService: ApiService) {
    this.editForm = new FormGroup({
      'name': new FormControl()
    });
    this.createForm = new FormGroup({
      'name': new FormControl()
    });
  }

  ngOnInit() {
    this.apiService.getDepartments().subscribe(res => {
      this.departments = res.data;
    });
  }

  delete(id: string) {
    this.apiService.deleteDepartment(id).subscribe(res => {
      if (res.success === true) {
        this.departments = res.data;
      } else {
        alert('Error');
      }
    });
  }

  edit(id: string) {
    this.editActive = id;
    this.cancelCreate();
    this.editForm.setValue({
      'name': this.departments.find(value => value.id === id).name
    });
  }

  create() {
    this.createActive = true;
    this.cancelEdit();
  }

  saveDepartment() {
    this.apiService.updateDepartment({
      'name': this.editForm.value.name
    }, this.editActive).subscribe(res => {
      if (res.success === true) {
        this.departments = res.data;
        this.editActive = null;
        this.editForm.reset();
      } else {
        console.log('error');
      }
    });
  }

  createDepartment() {
    this.apiService.createDepartment({
      'name': this.createForm.value.name
    }).subscribe(res => {
      if (res.success === true) {
        this.departments = res.data;
        this.createActive = false;
        this.createForm.reset();
      } else {
        console.log('error');
      }
    });
  }

  cancelEdit() {
    this.editActive = null;
    this.editForm.reset();
  }

  cancelCreate() {
    this.createActive = false;
    this.createForm.reset();
  }

}
