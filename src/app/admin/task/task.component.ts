import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {Tasks} from '../../models/resultmodels/task';
import {FormControl, FormGroup} from '@angular/forms';
import {Employee} from '../../models/resultmodels/employee';
import {Project} from '../../models/resultmodels/project';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  userTasks: Tasks[] = [];

  userId: string;

  createForm: FormGroup;
  createActive = false;

  editForm: FormGroup;
  editActive: string = null;

  employees: Employee[] = [];

  projects: Project[] = [];


  constructor(private apiService: ApiService) {
    this.createForm = new FormGroup({
      'name': new FormControl(),
      'description': new FormControl(),
      'type': new FormControl(),
      'priority': new FormControl(),
      'status': new FormControl(),
      'startDate': new FormControl(new Date().toISOString().slice(0, 16)),
      'endDate': new FormControl(new Date().toISOString().slice(0, 16)),
      'projectId': new FormControl(),
      'assignedEmployeeId': new FormControl()
    });
    this.editForm = new FormGroup({
      'name' : new FormControl(),
      'description': new FormControl(),
      'type': new FormControl(),
      'priority': new FormControl(),
      'status': new FormControl(),
      'startDate': new FormControl(new Date().toISOString().slice(0, 16)),
      'endDate': new FormControl(new Date().toISOString().slice(0, 16)),
      'projectId': new FormControl(),
      'assignedEmployeeId': new FormControl()
    });
  }

  create() {
    this.createActive = true;
  }

  cancelCreate() {
    this.createActive = false;
    this.createForm.reset();
  }

  cancelEdit() {
    this.editActive = null;
    this.editForm.reset();
  }

  getAccount() {
    this.apiService.getUser().subscribe(res => {
      this.userId = res.data.id;
    });
  }

  delete(id: string) {
    this.apiService.deleteTask(id).subscribe(res => {
      if (res.success === true) {
        this.userTasks = res.data;
      } else {
        alert('Error');
      }
    });
  }

  edit(id: string) {
    this.editActive = id;
    this.cancelCreate();
    const task = this.userTasks.find(value => value.id === id);
    this.editForm.setValue({
      'name': task.name,
      'description': task.description,
      'type': task.type,
      'priority': task.priority,
      'status': task.status,
      'startDate': new Date(task.startDate).toISOString().slice(0, 16),
      'endDate': new Date(task.endDate).toISOString().slice(0, 16),
      'assignedEmployeeId': task.assignedEmployeeId,
      'projectId': this.projects.find(value => value.name === task.projectName).id
    });
  }

  createTask() {
    this.apiService.createTask({
      'name': this.createForm.value.name,
      'description': this.createForm.value.description,
      'type': this.createForm.value.type,
      'priority': this.createForm.value.priority,
      'status': this.createForm.value.status,
      'startDate': new Date(this.createForm.value.startDate).toISOString().slice(0, 16),
      'endDate': new Date(this.createForm.value.endDate).toISOString().slice(0, 16),
      'projectId': this.createForm.value.projectId,
      'assignedEmployeeId': this.createForm.value.assignedEmployeeId,
    }).subscribe(res => {
      if (res.success === true) {
        this.userTasks = res.data;
        this.createActive = false;
        this.createForm.reset();
      } else {
        console.log('error');
      }
    });
  }

  saveTask() {
    this.apiService.updateTask({
      'name': this.editForm.value.name,
      'description': this.editForm.value.description,
      'type': this.editForm.value.type,
      'priority': this.editForm.value.priority,
      'status': this.editForm.value.status,
      'startDate': new Date(this.editForm.value.startDate).toISOString().slice(0, 16),
      'endDate': new Date(this.editForm.value.endDate).toISOString().slice(0, 16),
      'assignedEmployeeId': this.editForm.value.assignedEmployeeId,
      'projectId': this.editForm.value.projectId
    }, this.editActive).subscribe(res => {
      if (res.success === true) {
        this.userTasks = res.data;
          this.editActive = null;
          this.editForm.reset();
      } else {
        console.log('error');
      }
    });
  }

  ngOnInit() {
    this.getAccount();
    this.apiService.getUserTask().subscribe(res => {
      this.userTasks = res.data;
    });
    this.apiService.getAllUser().subscribe(res => {
      this.employees = res.data;
    });
    this.apiService.getAllProjects().subscribe(res => {
      this.projects = res.data;
    });
  }
}
