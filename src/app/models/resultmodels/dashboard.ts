export interface Dashboard {
  totalProject: number;
  totalEmployee: number;
  totalTask: number;
  totalLeaveRequest: number;
}
