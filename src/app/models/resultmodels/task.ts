export interface Tasks {
  id:                   string;
  name:                 string;
  description:          string;
  type:                 number;
  priority:             number;
  status:               number;
  startDate:            string;
  endDate:              string;
  projectName:          string;
  assignedEmployeeId:   string;
  assignerEmployeeName: string;
  assignedEmployeeName: string;
}
