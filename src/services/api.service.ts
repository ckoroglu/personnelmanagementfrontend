import 'rxjs/add/operator/map';
import {HttpClient} from '@angular/common/http';
import {RestResult} from '../app/models/rest-result';
import {Injectable} from '@angular/core';
import {Employee} from '../app/models/resultmodels/employee';
import {Dashboard} from '../app/models/resultmodels/dashboard';
import {Tasks} from '../app/models/resultmodels/task';
import {Department} from '../app/models/resultmodels/department';
import {LeaveType} from '../app/models/resultmodels/leave-type';
import {Leave} from '../app/models/resultmodels/leave';
import {Project} from '../app/models/resultmodels/project';

@Injectable()

export class ApiService {

  private BASE_URL = 'http://personnelmanagement-env.s36q9jsat3.eu-central-1.elasticbeanstalk.com/api/';
  private USER_URL = this.BASE_URL + 'Users';
  private LOGIN_URL = this.BASE_URL + 'Users/Login';
  private DASHBOARD_INFO_URL = this.BASE_URL + 'Users/DashboardInfo';
  private TASK_URL = this.BASE_URL + 'Task';
  private DEPARTMENT_URL = this.BASE_URL + 'Department';
  private LEAVE_TYPE_URL = this.BASE_URL + 'LeaveType';
  private LEAVE_URL = this.BASE_URL + 'Leave';
  private PROJECT_URL = this.BASE_URL + 'Project';


  constructor(private http: HttpClient) {
  }

  createUser(userRequest: any) {
    return this.http.post<RestResult<Employee[]>>(this.USER_URL, userRequest);
  }

  updateUser(userRequest: any, userId: string) {
    return this.http.put<RestResult<Employee[]>>(this.USER_URL + '/' + userId, userRequest);
  }

  deleteUser(userId: string) {
    return this.http.delete<RestResult<Employee[]>>(this.USER_URL + '/' + userId);
  }

  login(loginRequest: any) {
    return this.http.post<RestResult<any>>(this.LOGIN_URL, loginRequest);
  }

  getUser() {
    return this.http.get<RestResult<Employee>>(this.USER_URL);
  }

  getAllUser() {
    return this.http.get<RestResult<Employee[]>>(this.USER_URL + '/GetAll');
  }

  getDashboardInfo() {
    return this.http.get<RestResult<Dashboard>>(this.DASHBOARD_INFO_URL);
  }

  getUserTask() {
    return this.http.get<RestResult<Tasks[]>>(this.TASK_URL);
  }

  deleteTask(taskId: string) {
    return this.http.delete<RestResult<Tasks[]>>(this.TASK_URL + '/' + taskId);
  }

  updateTask(taskRequest: any, taskId: string) {
    return this.http.put<RestResult<Tasks[]>>(this.TASK_URL + '/' + taskId, taskRequest);
  }

  createTask(taskRequest: any) {
    return this.http.post<RestResult<Tasks[]>>(this.TASK_URL, taskRequest);
  }

  getDepartments() {
    return this.http.get<RestResult<Department[]>>(this.DEPARTMENT_URL);
  }

  createDepartment(departmentRequest: any) {
    return this.http.post<RestResult<Department[]>>(this.DEPARTMENT_URL, departmentRequest);
  }

  updateDepartment(departmentRequest: any, id: string) {
    return this.http.put<RestResult<Department[]>>(this.DEPARTMENT_URL + '/' + id, departmentRequest);
  }

  deleteDepartment(id: string) {
    return this.http.delete<RestResult<Department[]>>(this.DEPARTMENT_URL + '/' + id);
  }

  getLeaveTypes() {
    return this.http.get<RestResult<LeaveType[]>>(this.LEAVE_TYPE_URL);
  }

  createLeaveType(leaveTypeRequest: any) {
    return this.http.post<RestResult<LeaveType[]>>(this.LEAVE_TYPE_URL, leaveTypeRequest);
  }

  updateLeaveType(leaveTypeRequest: any, id: string) {
    return this.http.put<RestResult<LeaveType[]>>(this.LEAVE_TYPE_URL + '/' + id, leaveTypeRequest);
  }

  deleteLeaveType(id: string) {
    return this.http.delete<RestResult<LeaveType[]>>(this.LEAVE_TYPE_URL + '/' + id);
  }

  getAllLeaveRequest() {
    return this.http.get<RestResult<Leave[]>>(this.LEAVE_URL + '/AllLeaveRequests');
  }

  getUserLeaveRequest(id: string) {
    return this.http.get<RestResult<Leave[]>>(this.LEAVE_URL + '/UserLeaves/' + id);
  }

  getOwnLeaveRequest() {
    return this.http.get<RestResult<Leave[]>>(this.LEAVE_URL);
  }

  createLeaveRequest(leaveRequest: any) {
    return this.http.post<RestResult<Leave[]>>(this.LEAVE_URL, leaveRequest);
  }

  cancelLeaveRequest(id: string) {
    return this.http.delete<RestResult<Leave[]>>(this.LEAVE_URL + '/' + id);
  }

  evaluateLeaveRequest(id: string, leaveRequest: any) {
    return this.http.put<RestResult<Leave[]>>(this.LEAVE_URL + '/' + id, leaveRequest);
  }

  getAllProjects() {
    return this.http.get<RestResult<Project[]>>(this.PROJECT_URL + '/GetAllProjects');
  }

  getProjectTasks(projectId: string) {
    return this.http.get<RestResult<Project[]>>(this.PROJECT_URL + '/' + projectId + '/Tasks');
  }

  createProject(projectRequest: any) {
    return this.http.post<RestResult<Project[]>>(this.PROJECT_URL, projectRequest);
  }

  updateProject( projectRequest: any, projectId: string) {
    return this.http.put<RestResult<Project[]>>(this.PROJECT_URL + '/' + projectId, projectRequest);
  }

  deleteProject(projectId: string) {
    return this.http.delete<RestResult<Project[]>>(this.PROJECT_URL + '/' + projectId);
  }

}
