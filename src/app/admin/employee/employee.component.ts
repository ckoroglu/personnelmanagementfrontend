import {Component, OnInit} from '@angular/core';
import {Employee} from '../../models/resultmodels/employee';
import {ApiService} from '../../../services/api.service';
import {FormControl, FormGroup} from '@angular/forms';
import {Department} from '../../models/resultmodels/department';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  employees: Employee[] = [];
  departments: Department[] = [];

  editActive: string = null;
  editForm: FormGroup;

  createActive = false;
  createForm: FormGroup;

  constructor(private apiService: ApiService) {
    this.createForm = new FormGroup({
      'name': new FormControl(),
      'surname': new FormControl(),
      'email': new FormControl(),
      'password': new FormControl(),
      'address': new FormControl(),
      'gender': new FormControl(),
      'salary': new FormControl(),
      'startDate': new FormControl(),
      'phoneNumber': new FormControl(),
      'role': new FormControl(),
      'department': new FormControl(),
      'totalWorkingHours': new FormControl()
    });
    this.editForm = new FormGroup({
      'name': new FormControl(),
      'surname': new FormControl(),
      'email': new FormControl(),
      'password': new FormControl(),
      'address': new FormControl(),
      'gender': new FormControl(),
      'salary': new FormControl(),
      'startDate': new FormControl(),
      'phoneNumber': new FormControl(),
      'role': new FormControl(),
      'department': new FormControl(),
      'totalWorkingHours': new FormControl()
    });
  }

  create() {
    this.createActive = true;
    this.cancelEdit();
  }

  cancelCreate() {
    this.createActive = false;
    this.createForm.reset();
  }

  cancelEdit() {
    this.editActive = null;
    this.editForm.reset();
  }

  createEmployee() {
    this.apiService.createUser({
      'firstName': this.createForm.value.name,
      'lastName': this.createForm.value.surname,
      'email': this.createForm.value.email,
      'password': this.createForm.value.password,
      'address': this.createForm.value.address,
      'phoneNumber': this.createForm.value.phoneNumber,
      'gender': this.createForm.value.gender,
      'salary': this.createForm.value.salary,
      'workingDayPerMonth': this.createForm.value.totalWorkingHours,
      'role': this.createForm.value.role,
      'departmentId': this.createForm.value.department,
      'startDate': this.createForm.value.startDate
    }).subscribe(res => {
      if (res.success === true) {
        this.employees = res.data;
        this.createActive = false;
        this.createForm.reset();
      } else {
        console.log('error');
      }
    });
  }

  delete(id: string) {
    this.apiService.deleteUser(id).subscribe(res => {
      if (res.success === true) {
        this.employees = res.data;
      } else {
        alert('Error');
      }
    });
  }

  edit(id: any) {
    this.editActive = id;
    this.cancelCreate();
    const employee = this.employees.find(value => value.id === id);
    this.editForm.setValue({
      'name': employee.firstName,
      'surname': employee.lastName,
      'email': employee.email,
      'password': '******',
      'address': employee.address,
      'gender': employee.gender,
      'salary': employee.salary,
      'phoneNumber': employee.phoneNumber,
      'role': employee.role,
      'department': this.departments.find(value => value.name === employee.department).id,
      'totalWorkingHours': employee.workingDayPerMonth,
      'startDate': new Date(employee.startDate).toISOString().slice(0, 16),
    });
  }

  saveEmployee() {
    this.apiService.updateUser({
      'firstName': this.editForm.value.name,
      'lastName': this.editForm.value.surname,
      'email': this.editForm.value.email,
      'password': this.editForm.value.password,
      'address': this.editForm.value.address,
      'phoneNumber': this.editForm.value.phoneNumber,
      'gender': this.editForm.value.gender,
      'startDate': this.editForm.value.startDate,
      'salary': this.editForm.value.salary,
      'workingDayPerMonth': this.editForm.value.totalWorkingHours,
      'role': this.editForm.value.role,
      'departmentId': this.editForm.value.department
    }, this.editActive).subscribe(res => {
      if (res.success === true) {
        this.employees = res.data;
        this.editActive = null;
        this.editForm.reset();
      } else {
        console.log('error');
      }
    });
  }

  ngOnInit() {
    this.apiService.getDepartments().subscribe(res => {
      this.departments = res.data;
    });
    this.apiService.getAllUser().subscribe(res => {
      this.employees = res.data;
    });
  }
}
