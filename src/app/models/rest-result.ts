export interface RestResult<T> {
  success: boolean;
  errors: string;
  data: T;
}
