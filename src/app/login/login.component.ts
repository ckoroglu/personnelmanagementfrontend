import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../services/api.service';
import {Router} from '@angular/router';
import {GlobalResults} from '../auth/global-results';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formLogin: FormGroup;
  loginError = false;

  constructor(private apiService: ApiService, private routerService: Router, private globalService: GlobalResults) { }

  ngOnInit() {
    this.formLogin = new FormGroup({
      username: new FormControl('', Validators.compose([Validators.required, Validators.email])),
      password: new FormControl('', Validators.required)
    });
    if (localStorage.getItem('token')) {
      this.routerService.navigate(['/app/home']);
    }
  }

  login() {
    this.loginError = false;
    if (!this.formLogin.valid) {
      this.loginError = true;
      return;
    }
    const loginRequest: any = {
      email: this.formLogin.value.username,
      password: this.formLogin.value.password,
    };
    this.apiService.login(loginRequest).subscribe(res => {
      if (res.success === true) {
        localStorage.setItem('token', res.data);
        this.apiService.getUser().subscribe(value => {
          this.globalService.account = value.data;
          localStorage.setItem('role', this.globalService.account.role);
          this.routerService.navigate(['/app/home']);
        });
      } else {
        this.loginError = true;
      }
    });
  }

}
