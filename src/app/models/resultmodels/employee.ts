export interface Employee {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  startDate: string;
  address: string;
  phoneNumber: string;
  gender: number;
  status: number;
  salary: number;
  role: string;
  department: string;
  workingDayPerMonth: number;
}
