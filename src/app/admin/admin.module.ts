import { AdminRoutingModule } from './admin-routing/admin-routing.module';
import { AdminDashboard1Component } from './admin-dashboard1/admin-dashboard1.component';
import { AdminControlSidebarComponent } from './admin-control-sidebar/admin-control-sidebar.component';
import { AdminFooterComponent } from './admin-footer/admin-footer.component';
import { AdminContentComponent } from './admin-content/admin-content.component';
import { AdminLeftSideComponent } from './admin-left-side/admin-left-side.component';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { AdminComponent } from './admin.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import {GlobalResults} from '../auth/global-results';
import { TaskComponent } from './task/task.component';
import { DepartmentComponent } from './department/department.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { UserLeaveComponent } from './user-leave/user-leave.component';
import { ProjectsComponent } from './projects/projects.component';
import { EmployeeComponent } from './employee/employee.component';
import { LeavetypeComponent } from './leavetype/leavetype.component';
import { LeaveRequestComponent } from './leave-request/leave-request.component';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    AdminComponent,
    AdminHeaderComponent,
    AdminLeftSideComponent,
    AdminContentComponent,
    AdminFooterComponent,
    AdminControlSidebarComponent,
    AdminDashboard1Component,
    TaskComponent,
    DepartmentComponent,
    UserLeaveComponent,
    ProjectsComponent,
    EmployeeComponent,
    LeavetypeComponent,
    LeaveRequestComponent,
  ],
  exports: [AdminComponent],
  providers: [GlobalResults]
})
export class AdminModule { }
