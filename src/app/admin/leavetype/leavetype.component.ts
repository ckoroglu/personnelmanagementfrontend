import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {ApiService} from '../../../services/api.service';
import {LeaveType} from '../../models/resultmodels/leave-type';

@Component({
  selector: 'app-leavetype',
  templateUrl: './leavetype.component.html',
  styleUrls: ['./leavetype.component.css']
})
export class LeavetypeComponent implements OnInit {

  leavetypes: LeaveType[] = [];

  editActive: string = null;
  editForm: FormGroup;

  createActive = false;
  createForm: FormGroup;

  constructor(private apiService: ApiService) {
    this.editForm = new FormGroup({
      'name': new FormControl(),
      'type': new FormControl()
    });
    this.createForm = new FormGroup({
      'name': new FormControl(),
      'type': new FormControl('true')
    });
  }

  ngOnInit() {
    this.apiService.getLeaveTypes().subscribe(res => {
      this.leavetypes = res.data;
    });
  }

  delete(id: string) {
    this.apiService.deleteLeaveType(id).subscribe(res => {
      if (res.success === true) {
        this.leavetypes = res.data;
      } else {
        alert('Error');
      }
    });
  }

  edit(id: string) {
    this.editActive = id;
    this.cancelCreate();
    const leaveType = this.leavetypes.find(value => value.id === id);
    this.editForm.setValue({
      'name': leaveType.name,
      'type': leaveType.type ? 'true' : 'false'
    });
  }

  create() {
    this.createActive = true;
    this.cancelEdit();
  }

  saveLeaveType() {
    this.apiService.updateLeaveType({
      'name': this.editForm.value.name,
      'type': this.editForm.value.type
    }, this.editActive).subscribe(res => {
      if (res.success === true) {
        this.leavetypes = res.data;
        this.editActive = null;
      } else {
        console.log('error');
      }
    });
  }

  createLeaveType() {
    this.apiService.createLeaveType({
      'name': this.createForm.value.name,
      'type': this.createForm.value.type
    }).subscribe(res => {
      if (res.success === true) {
        this.leavetypes = res.data;
        this.createActive = false;
      } else {
        console.log('error');
      }
    });
  }

  cancelEdit() {
    this.editActive = null;
  }

  cancelCreate() {
    this.createActive = false;
  }
}
