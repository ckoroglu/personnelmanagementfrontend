import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {Leave} from '../../models/resultmodels/leave';
import {FormControl, FormGroup} from '@angular/forms';
import {LeaveType} from '../../models/resultmodels/leave-type';
import {Employee} from '../../models/resultmodels/employee';

@Component({
  selector: 'app-user-leave',
  templateUrl: './user-leave.component.html',
  styleUrls: ['./user-leave.component.css']
})
export class UserLeaveComponent implements OnInit {

  userLeaves: Leave[] = [];
  leaveTypes: LeaveType[] = [];
  createActive = false;

  payCut = 0;

  user: Employee = null;

  createForm: FormGroup;
  workDay: number;

  selectedLeaveType = false;

  constructor(private apiService: ApiService) {
    this.createForm = new FormGroup({
      'reason': new FormControl(),
      'startDate': new FormControl(new Date().toISOString().slice(0, 16)),
      'endDate': new FormControl(new Date().toISOString().slice(0, 16)),
      'leaveTypeId': new FormControl()
    });
  }

  ngOnInit() {
    this.apiService.getLeaveTypes().subscribe(res => {
      this.leaveTypes = res.data;
    });
    this.apiService.getOwnLeaveRequest().subscribe(res => {
      this.userLeaves = res.data;
    });
    this.apiService.getUser().subscribe(res => {
      this.user = res.data;
    });
    this.getBusinessDatesCount(new Date().toISOString().slice(0, 16), new Date().toISOString().slice(0, 16));
  }

  cancelCreate() {
    this.createActive = false;
    this.createForm.reset();
    this.payCut = 0;
  }

  createLeaveRequest() {
    this.apiService.createLeaveRequest({
      'reason': this.createForm.value.reason,
      'startDate': this.createForm.value.startDate,
      'endDate': this.createForm.value.endDate,
      'leaveTypeId': this.createForm.value.leaveTypeId
    }).subscribe(res => {
      if (res.success === true) {
        this.userLeaves = res.data;
        this.createActive = false;
        this.createForm.reset();
      } else {
        console.log('error');
      }
    });
  }

  create() {
    this.payCut = 0;
    this.selectedLeaveType = false;
    this.createActive = true;
  }

  delete(leaveId: string) {
    this.apiService.cancelLeaveRequest(leaveId).subscribe(res => {
      this.userLeaves = res.data;
    });
  }

  getBusinessDatesCount(startDate, endDate) {
    let count = 0;
    const curDate = new Date(startDate);
    while (curDate <= new Date(endDate)) {
      const dayOfWeek = curDate.getDay();
      if (!((dayOfWeek === 6) || (dayOfWeek === 0))) {
        count++;
      }
      curDate.setDate(curDate.getDate() + 1);
    }
    this.workDay = count;
    if (this.createForm.value.leaveTypeId != null) {
      this.changeLeaveType(this.createForm.value.leaveTypeId);
    }
  }

  changeLeaveType(leaveId: string) {
    this.selectedLeaveType = this.leaveTypes.find(value => value.id === leaveId).type;
    const salary = this.user.salary;
    const workDayPerMonth = this.user.workingDayPerMonth;
    this.payCut = ((this.workDay * salary) / workDayPerMonth);
  }

}
