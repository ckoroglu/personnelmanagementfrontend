import {Component, OnInit} from '@angular/core';
import {Leave} from '../../models/resultmodels/leave';
import {ApiService} from '../../../services/api.service';
import {FormControl, FormGroup} from '@angular/forms';
import {LeaveType} from '../../models/resultmodels/leave-type';

@Component({
  selector: 'app-leave-request',
  templateUrl: './leave-request.component.html',
  styleUrls: ['./leave-request.component.css']
})
export class LeaveRequestComponent implements OnInit {

  leaves: Leave[] = [];
  leaveTypes: LeaveType[] = [];
  evaluateActive = null;

  evaluateForm: FormGroup;

  workDay: number;

  constructor(private apiService: ApiService) {
    this.evaluateForm = new FormGroup({
      'remark': new FormControl(),
      'status': new FormControl(1)
    });
  }

  ngOnInit() {
    this.apiService.getLeaveTypes().subscribe(res => {
      this.leaveTypes = res.data;
    });
    this.apiService.getAllLeaveRequest().subscribe(res => {
      this.leaves = res.data;
    });
    this.getBusinessDatesCount(new Date().toISOString().slice(0, 16), new Date().toISOString().slice(0, 16));
  }

  delete(leaveId: string) {
    this.apiService.cancelLeaveRequest(leaveId).subscribe(res => {
      this.leaves = res.data;
    });
  }

  evaluate(requestId: string) {
    this.evaluateActive = requestId;
  }

  cancelEvaluation() {
    this.evaluateActive = null;
    this.evaluateForm.reset();
  }

  evaluateLeaveRequest() {
    this.apiService.evaluateLeaveRequest(this.evaluateActive,{
      'remark': this.evaluateForm.value.remark,
      'status': this.evaluateForm.value.status
    }).subscribe(res => {
      if (res.success === true) {
        this.leaves = res.data;
        this.evaluateActive = null;
        this.evaluateForm.reset();
      } else {
        console.log('error');
      }
    });
  }

  getBusinessDatesCount(startDate, endDate) {
    let count = 0;
    const curDate = new Date(startDate);
    while (curDate <= new Date(endDate)) {
      const dayOfWeek = curDate.getDay();
      if (!((dayOfWeek === 6) || (dayOfWeek === 0))) {
        count++;
      }
      curDate.setDate(curDate.getDate() + 1);
    }
    this.workDay = count;
  }

}
