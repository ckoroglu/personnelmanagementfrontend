import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {Project} from '../../models/resultmodels/project';
import {FormControl, FormGroup} from '@angular/forms';
import {GlobalResults} from '../../auth/global-results';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  projects: Project[] = [];

  userId: string;

  editActive: string = null;
  editForm: FormGroup;

  createActive = false;
  createForm: FormGroup;

  constructor(private apiService: ApiService, private globalService: GlobalResults) {

    this.editForm = new FormGroup({
      'name': new FormControl(),
      'description': new FormControl(),
      'status': new FormControl(),
      'startDate': new FormControl(),
      'endDate': new FormControl()
    });

    this.createForm = new FormGroup({
      'name': new FormControl(),
      'description': new FormControl(),
      'status': new FormControl(),
      'startDate': new FormControl(new Date().toISOString().slice(0, 16)),
      'endDate': new FormControl(new Date().toISOString().slice(0, 16))
    });
  }

  create() {
    this.createActive = true;
  }

  cancelCreate() {
    this.createActive = false;
    this.createForm.reset();
  }

  cancelEdit() {
    this.editActive = null;
    this.editForm.reset();
  }

  getAccount() {
    this.apiService.getUser().subscribe(res => {
      this.userId = res.data.id;
    });
  }

  createProject() {
    this.apiService.createProject({
      'name': this.createForm.value.name,
      'description': this.createForm.value.description,
      'startDate': this.createForm.value.startDate,
      'endDate': this.createForm.value.endDate,
      'createdBy': this.userId
    }).subscribe(res => {
      if (res.success === true) {
        this.projects = res.data;
        this.createActive = false;
        this.createForm.reset();
      }else {
        console.log('error');
      }
    });
  }

  delete(id: string) {
    this.apiService.deleteProject(id).subscribe(res => {
      if (res.success === true) {
        this.projects = res.data;
      } else {
        alert('Error');
      }
    });
  }

  edit(id: string) {
    this.editActive = id;
    this.cancelCreate();
    const project = this.projects.find(value => value.id === id);
    this.editForm.setValue({
      'name': project.name,
      'description': project.description,
      'status': project.status,
      'startDate': new Date(project.startDate).toISOString().slice(0, 16),
      'endDate': new Date(project.endDate).toISOString().slice(0, 16)
    });
  }

  saveProject() {
    this.apiService.updateProject({
      'name': this.editForm.value.name,
      'description': this.editForm.value.description,
      'status': this.editForm.value.status,
      'startDate':  new Date(this.editForm.value.startDate).toISOString(),
      'endDate':  new Date(this.editForm.value.endDate).toISOString()
    }, this.editActive).subscribe(res => {
      if (res.success === true) {
        this.projects = res.data;
        this.editActive = null;
        this.editForm.reset();
      } else {
        console.log('error');
      }
    });
  }

  ngOnInit() {
    this.getAccount();
    this.apiService.getAllProjects().subscribe(res => {
      this.projects = res.data;
    });
  }

}
