export interface Leave {
  id:           string;
  reason:       string;
  createdAt:    string;
  startDate:    string;
  endDate:      string;
  remark:       string;
  status:       number;
  managerName:  string;
  leaveType:    string;
  employeeName: string;
}
