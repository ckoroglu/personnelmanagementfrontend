import { Component, OnInit } from '@angular/core';
import {GlobalResults} from '../../auth/global-results';
import {Employee} from '../../models/resultmodels/employee';
import {ApiService} from '../../../services/api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.css']
})
export class AdminHeaderComponent implements OnInit {

  user: Employee = null;

  constructor(private globalService: GlobalResults, private apiService: ApiService, private routerService: Router) {
  }

  ngOnInit() {
    if (localStorage.getItem('token')) {
      this.getAccount();
    }
  }

  getAccount() {
    this.apiService.getUser().subscribe(res => {
      this.user = res.data;
      this.globalService.account = res.data;
    });
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    this.routerService.navigate(['/login']);
  }

}
